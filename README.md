# InvestmentPlatformPV293

This is a one man show project that simulates DDD design which comprises of two bounded context - monitoring context and portfolio context.
The communication between them follows this use case:

<b>Portfolio context:</b>
Is used for creating portfolios, buying and selling holdings.

<b>Monitoring context:</b>
Observes market changes and informs the rest of the system about them

<b>Monitoring portfolio relation:</b> 
Monitoring context works as an observer of market changes and informs portfolio context about them. The portfolio context has configured trades, applicable once the price decreases. If the market change is appropriate for the configured trade, the trade is automatically conducted.