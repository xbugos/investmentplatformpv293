using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using MediatR;
using MediatR.Pipeline;
using Portfolio.src.Infrastructure.Queries;
using Portfolio.src.Infrastructure.Repos.Interfaces;
using Portfolio.src.Infrastructure.Repos.Repositories;
using Portfolio.src.UseCases.Holding.Buy;
using Portfolio.src.UseCases.Holding.List;

namespace Portfolio.src.UI
{
    public class AutofacInfrastructureModule : Autofac.Module
{
  private readonly bool _isDevelopment = false;
  private readonly List<Assembly> _assemblies = [];

  public AutofacInfrastructureModule(bool isDevelopment, Assembly? callingAssembly = null)
  {
    _isDevelopment = isDevelopment;
    AddToAssembliesIfNotNull(callingAssembly);
  }

  private void AddToAssembliesIfNotNull(Assembly? assembly)
  {
    if(assembly != null)
    {
      _assemblies.Add(assembly);
    }
  }

  private void LoadAssemblies()
  {
    // TODO: Replace these types with any type in the appropriate assembly/project
    // var coreAssembly = Assembly.GetAssembly(typeof(Project)); 
    // var infrastructureAssembly = Assembly.GetAssembly(typeof(AutofacInfrastructureModule));
    // var useCasesAssembly = Assembly.GetAssembly(typeof(CreateContributorCommand));

    // AddToAssembliesIfNotNull(coreAssembly);
    // AddToAssembliesIfNotNull(infrastructureAssembly);
    // AddToAssembliesIfNotNull(useCasesAssembly);
  }

  protected override void Load(ContainerBuilder builder)
  {
    LoadAssemblies();
    if (_isDevelopment)
    {
      RegisterDevelopmentOnlyDependencies(builder);
    }
    else
    {
      RegisterProductionOnlyDependencies(builder);
    }
    RegisterEF(builder);
    RegisterMediatR(builder);
  }

  private void RegisterEF(ContainerBuilder builder)
  {
    // builder.RegisterGeneric(typeof(EfRepository<>))
    //   .As(typeof(IRepository<>))
    //   .As(typeof(IReadRepository<>))
    //   .InstancePerLifetimeScope();
  }

  private void RegisterMediatR(ContainerBuilder builder)
  {
    builder
      .RegisterType<Mediator>()
      .As<IMediator>()
      .InstancePerLifetimeScope();

    // builder
    //   .RegisterGeneric(typeof(LoggingBehavior<,>))
    //   .As(typeof(IPipelineBehavior<,>))
    //   .InstancePerLifetimeScope();

    // builder
    //   .RegisterType<MediatRDomainEventDispatcher>()
    //   .As<IDomainEventDispatcher>()
    //   .InstancePerLifetimeScope();

    var mediatrOpenTypes = new[]
    {
      typeof(IRequestHandler<,>),
      typeof(IRequestExceptionHandler<,,>),
      typeof(IRequestExceptionAction<,>),
      typeof(INotificationHandler<>),
    };

    foreach (var mediatrOpenType in mediatrOpenTypes)
    {
      builder
        .RegisterAssemblyTypes(typeof(BuyHoldingCommand).GetTypeInfo().Assembly)
        .AsClosedTypesOf(mediatrOpenType)
        .AsImplementedInterfaces();
    }
  }

  private void RegisterDevelopmentOnlyDependencies(ContainerBuilder builder)
  {
    // NOTE: Add any development only services here
    //builder.RegisterType<FakeEmailSender>().As<IEmailSender>()
    //  .InstancePerLifetimeScope();

    // NOTE: Add any production only (real) services here
    // builder.RegisterType<SmtpEmailSender>().As<IEmailSender>()
    //   .InstancePerLifetimeScope();


    builder.RegisterType<CustomerRepository>()
         .As<ICustomerRepository>()
         .InstancePerLifetimeScope();
    
    builder.RegisterType<HoldingRepository>()
      .As<IHoldingRepository>()
      .InstancePerLifetimeScope();

    builder.RegisterType<ListHoldingsQueryService>()
          .As<IListHoldingsQueryService>()
          .InstancePerLifetimeScope();

    // builder.RegisterType<FakeListIncompleteItemsQueryService>()
    //   .As<IListIncompleteItemsQueryService>()
    //   .InstancePerLifetimeScope();

    // builder.RegisterType<FakeListProjectsShallowQueryService>()
    //   .As<IListProjectsShallowQueryService>()
    //   .InstancePerLifetimeScope();
  }

  private void RegisterProductionOnlyDependencies(ContainerBuilder builder)
  {
    // NOTE: Add any production only (real) services here
    // builder.RegisterType<SmtpEmailSender>().As<IEmailSender>()
    //   .InstancePerLifetimeScope();

    // builder.RegisterType<ListContributorsQueryService>()
    //   .As<IListContributorsQueryService>()
    //   .InstancePerLifetimeScope();

    // builder.RegisterType<ListIncompleteItemsQueryService>()
    //   .As<IListIncompleteItemsQueryService>()
    //   .InstancePerLifetimeScope();

    // builder.RegisterType<ListProjectsShallowQueryService>()
    //   .As<IListProjectsShallowQueryService>()
    //   .InstancePerLifetimeScope();

    // builder.RegisterType<ListHoldingsQuery>()
    //        .As<IListHoldingsQueryService>()
    //        .InstancePerLifetimeScope();
  }
}

}