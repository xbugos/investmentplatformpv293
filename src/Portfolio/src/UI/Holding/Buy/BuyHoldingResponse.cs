using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.src.UI.Holding.Buy
{
    public record BuyHoldingResponse(Guid Id, string Name, int Amount){}
}