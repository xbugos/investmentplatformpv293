using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.src.UI.Holding.Buy
{
    public record BuyHoldingRequest(Guid BuyerId, Guid HoldingId, int Amount) {}
}