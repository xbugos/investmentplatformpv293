using FastEndpoints;
using MediatR;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Portfolio.src.Core.Helpers.Exceptions;
using Portfolio.src.UseCases.Holding.Buy;

namespace Portfolio.src.UI.Holding.Buy
{
    public class Buy : Endpoint<BuyHoldingRequest, BuyHoldingResponse>
    {

        private IMediator _mediator;
        public Buy(IMediator mediator)
        {
            _mediator = mediator;
        }

        public override void Configure()
        {
            Post("holding/buy");
            // only till there is not authentication
            AllowAnonymous();
            Summary(s => s.ExampleRequest = new BuyHoldingRequest(Guid.NewGuid(), Guid.NewGuid(), 5));
        } 

        public override async Task HandleAsync(BuyHoldingRequest req, CancellationToken ct)
        {
            // TODO: logging
            // TODO: custom code
            var holding = await _mediator.Send(new BuyHoldingCommand(req.BuyerId, req.HoldingId, req.Amount));
            Response = new BuyHoldingResponse(holding.HoldingId, holding.Name, holding.Amount);
            return;
        }
    }
}