using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.src.UseCases.Holding.Common;
using Portfolio.src.UseCases.Holding.List;

namespace Portfolio.src.UI.Holding.List
{
    public class ListHoldingsResponse
    {
        public List<HoldingListDto> HoldingListDtos {get; set;} = new();
    }
}