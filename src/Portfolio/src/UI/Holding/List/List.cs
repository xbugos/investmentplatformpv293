using FastEndpoints;
using MediatR;
using Portfolio.src.Infrastructure.Repos.Interfaces;
using Portfolio.src.UseCases.Holding.Common;
using Portfolio.src.UseCases.Holding.List;

namespace Portfolio.src.UI.Holding.List
{
    public class List : EndpointWithoutRequest<ListHoldingsResponse>
    {

        private IMediator _mediator;

        private IHoldingRepository _holdingRepository;
        
        public List(IMediator mediator, IHoldingRepository holdingRepository)
        {
            _mediator = mediator;
            _holdingRepository = holdingRepository;
        }

        public override void Configure()
        {
            Get("/holding");
            // only till there is not authentication
            AllowAnonymous();
        }

        public override async Task HandleAsync(CancellationToken ct)
        {
            // TODO: logging
            // TODO: custom code
            var result = await _mediator.Send(new ListHoldingsQuery());
            Response = new ListHoldingsResponse() {HoldingListDtos = result.ToList()};
        }
    }
}