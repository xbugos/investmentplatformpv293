using FastEndpoints;
using MediatR;
using Portfolio.src.Core.Helpers.Exceptions;
using Portfolio.src.UseCases.Holding.Sell;

namespace Portfolio.src.UI.Holding.Sell
{
    public class Sell : Endpoint<SellHoldingRequest, SellHoldingResponse>
    {

        private IMediator _mediator;
        public Sell(IMediator mediator)
        {
            _mediator = mediator;
        }

        public override void Configure()
        {
            Post("holding/sell");
            // only till there is not authentication
            AllowAnonymous();
            Summary(s => s.ExampleRequest = new SellHoldingRequest(Guid.NewGuid(), Guid.NewGuid(), 5, DateTime.Now));
        }

        public override async Task HandleAsync(SellHoldingRequest req, CancellationToken ct)
        {
            // TODO: logging
            try {
                var holding = await _mediator.Send(new SellHoldingCommand(req.sellerId, req.holdingId, req.amount));
                Response = new SellHoldingResponse(holding.HoldingId, holding.Name, holding.Amount);
                return;
            } catch (CannotSellHoldingException) {
                // TODO: finish it
                return;
            }
        }
    }
}