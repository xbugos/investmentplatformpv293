using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.src.UI.Holding.Sell
{
    public record SellHoldingRequest(Guid sellerId, Guid holdingId, int amount, DateTime timeStamp);
}