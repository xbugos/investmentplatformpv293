using Portfolio.src.Core.Common;
using Portfolio.src.Core.Events;
using Portfolio.src.Core.Helpers.Exceptions;

namespace Portfolio.src.Core.Aggregates.HoldingAggregate;

public class Holding : BaseEntity
{
    public Guid Id {get; private set; }

    public String Name {get; private set;}
    
    private readonly Guid _buyerId;

    private readonly List<Trade> _trades;
    public IReadOnlyCollection<Trade> Trades => _trades.AsReadOnly();

    
    public int TotalAmount => Trades.Aggregate(0, 
        (acc, trade) =>  trade.TradeTypeEnum == TradeTypeEnum.Buy ? acc + trade.Amount : acc - trade.Amount);

    public static Holding NewHolding(Guid id, string name) {
        return new Holding(id, name);
    }

    private Holding(Guid id, string name)
    {
        Id = id;
        Name = name;
        _trades = new List<Trade>();

        this.AddDomainEvent(new HoldingWasCreatedEvent(this));
    }

    public Trade AddTrade(TradeTypeEnum tradeTypeEnum, int amount) {
        // validate provided daa
        // amount is not lower than 1 etc. 

        var trade = new Trade(tradeTypeEnum, amount);

        if (trade.TradeTypeEnum == TradeTypeEnum.Sell && trade.Amount > TotalAmount) {
            throw new CannotSellHoldingException(trade.Amount, TotalAmount);
        }
        _trades.Add(trade);

        this.AddDomainEvent(new HoldingBoughtEvent(trade));
        
        return trade;
        // publish event
    }
}