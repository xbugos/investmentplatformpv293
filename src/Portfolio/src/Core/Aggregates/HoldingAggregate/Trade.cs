
namespace Portfolio.src.Core.Aggregates.HoldingAggregate
{
    public class Trade
    {
        public TradeTypeEnum TradeTypeEnum {get; set;}

        public int Amount {get; set;}

        public Trade(TradeTypeEnum tradeTypeEnum, int amount) {
            TradeTypeEnum = tradeTypeEnum;
            Amount = amount;
        }
    }
}