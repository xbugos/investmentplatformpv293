using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.src.Core.Common;

namespace Portfolio.src.Core.Aggregates.PortfolioAggregate
{
    public class Portfolio : BaseEntity
    {
        public string Name { get; private set;}
        public string Description {get; private set;}

        public Portfolio(string name, string description) {
            Name = name;
            Description = description;
        }
    }
}