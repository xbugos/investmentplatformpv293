using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.src.Core.Aggregates.HoldingAggregate;
using Portfolio.src.Core.Common;

namespace Portfolio.src.Core.Events
{
    public class HoldingBoughtEvent : BaseEvent
    {
        public Trade Trade {get;}

        public HoldingBoughtEvent(Trade trade) {
            this.Trade = trade;
        }
    }
}