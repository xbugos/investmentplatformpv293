using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FastEndpoints;
using Microsoft.Net.Http.Headers;
using Portfolio.src.Core.Aggregates.HoldingAggregate;
using Portfolio.src.Core.Common;

namespace Portfolio.src.Core.Events
{
    public class HoldingWasCreatedEvent : BaseEvent
    {
        public Holding Holding {get; private set;}

        public HoldingWasCreatedEvent(Holding holding) {
            Holding = holding;
        }

    }
}