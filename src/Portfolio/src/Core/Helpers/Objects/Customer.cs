using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.src.Core.Helpers.Objects
{
    public class Customer
    {
        private Guid _id;
        public Guid Id => _id;
        
        private string _name;
        public string Name => _name;
    
        private string _surname;
        public string Surname => _surname;

        public Customer (Guid id, string name, string surname) {
            _id = id;
            _name = name;
            _surname = surname;
        }
    }
}