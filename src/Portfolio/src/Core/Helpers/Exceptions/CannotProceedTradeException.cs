using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace Portfolio.src.Core.Helpers.Exceptions
{
    public class CannotSellHoldingException : Exception
    {
        public int _expectedAmount {get; private set;}
        public int _currentAmount {get; private set;}
        public override string Message => "Expected amount of holding is higher than current amount";

        public CannotSellHoldingException(int expectedAmount, int currentAmount) {
            _expectedAmount = expectedAmount;
            _currentAmount = currentAmount;
        }
    }
}