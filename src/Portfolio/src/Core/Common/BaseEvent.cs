using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace Portfolio.src.Core.Common
{
    public abstract class BaseEvent : INotification
    {
        public DateTime TimeStamp {get; protected set;} = DateTime.UtcNow;
    }
}
