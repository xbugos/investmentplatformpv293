using MediatR;
using Portfolio.src.Core.Aggregates.HoldingAggregate;
using Portfolio.src.Core.Events;
using Portfolio.src.Infrastructure.Repos.Interfaces;
using Portfolio.src.UI.Holding.Buy;
using Portfolio.src.UseCases.Holding.Common;


namespace Portfolio.src.UseCases.Holding.Buy
{
    public class BuyHoldingCommandHandler : IRequestHandler<BuyHoldingCommand, HoldingDto>
    {

        private readonly ICustomerRepository _customerRepository;
        private readonly IHoldingRepository _holdingRepository;

        public BuyHoldingCommandHandler(ICustomerRepository customerRepository, IHoldingRepository holdingRepository) {
            _customerRepository = customerRepository;
            _holdingRepository = holdingRepository;
        }

        public async Task<HoldingDto> Handle(BuyHoldingCommand request, CancellationToken cancellationToken)
        {
            var client = _customerRepository.Find(request.BuyerId);

            if (client == null) {
                throw new Exception("Client id does not exist.");
            }

            var holding = _holdingRepository.Find(request.HoldingId);
            if (holding == null) {
                holding = _holdingRepository.Create(holding: Portfolio.src.Core.Aggregates.HoldingAggregate.Holding.NewHolding(request.HoldingId, ""));
            }
            
            holding.AddTrade(TradeTypeEnum.Buy, request.amount);

            // TODO: add trade to repository : save changes async

            return new HoldingDto { HoldingId = holding.Id, Name = holding.Name, Amount = request.amount};
        }
    }
}