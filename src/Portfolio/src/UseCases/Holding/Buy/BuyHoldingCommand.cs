using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Portfolio.src.UseCases.Holding.Common;

namespace Portfolio.src.UseCases.Holding.Buy
{
    public record BuyHoldingCommand(Guid BuyerId, Guid HoldingId, int amount) : IRequest<HoldingDto>;
}