using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Portfolio.src.UseCases.Holding.Common;

namespace Portfolio.src.UseCases.Holding.Sell
{
    public record SellHoldingCommand(Guid BuyerId, Guid HoldingId, int amount) : IRequest<HoldingDto>;
}