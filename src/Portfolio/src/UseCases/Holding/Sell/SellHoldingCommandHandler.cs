using MediatR;
using Portfolio.src.Core.Aggregates.HoldingAggregate;
using Portfolio.src.Infrastructure.Repos.Interfaces;
using Portfolio.src.UseCases.Holding.Common;

namespace Portfolio.src.UseCases.Holding.Sell
{
    public class SellHoldingCommandHandler : IRequestHandler<SellHoldingCommand, HoldingDto>
    {

        private readonly ICustomerRepository _customerRepository;
        private readonly IHoldingRepository _holdingRepository;

        public SellHoldingCommandHandler(ICustomerRepository customerRepository, IHoldingRepository holdingRepository) {
            _customerRepository = customerRepository;
            _holdingRepository = holdingRepository;
        }

        public async Task<HoldingDto> Handle(SellHoldingCommand request, CancellationToken cancellationToken)
        {
            var client = _customerRepository.Find(request.BuyerId);

            if (client == null) {
                throw new Exception("Client id does not exist.");
            }

            var holding = _holdingRepository.Find(request.HoldingId);
            if (holding == null) {
                throw new Exception("Could not be processed due to non existing holding");
            }
            holding.AddTrade(TradeTypeEnum.Sell, request.amount);
            // TODO: save changes async

            return new HoldingDto{HoldingId = holding.Id, Name = holding.Name, Amount = request.amount };
        }
    }
}