using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.src.UseCases.Holding.List
{
    public interface IListHoldingsQueryService
    {
        Task<IEnumerable<HoldingListDto>> ListAsync();
    }
}