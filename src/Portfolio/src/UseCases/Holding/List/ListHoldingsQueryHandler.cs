using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;

namespace Portfolio.src.UseCases.Holding.List
{
    public class ListHoldingsQueryHandler : IRequestHandler<ListHoldingsQuery, IEnumerable<HoldingListDto>>
    {
        private readonly IListHoldingsQueryService _listHoldingQueryService;

        public ListHoldingsQueryHandler(IListHoldingsQueryService listHoldingsQueryService)
        {
            _listHoldingQueryService = listHoldingsQueryService;
        }

        public Task<IEnumerable<HoldingListDto>> Handle(ListHoldingsQuery request, CancellationToken cancellationToken)
        {
            return _listHoldingQueryService.ListAsync();
        }
    }
}