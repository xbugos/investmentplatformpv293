using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity.Data;

namespace Portfolio.src.UseCases.Holding.List
{
    public class ListHoldingsQuery : IRequest<IEnumerable<HoldingListDto>>
    {
        
    }
}