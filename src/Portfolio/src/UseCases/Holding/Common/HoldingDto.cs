using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portfolio.src.UseCases.Holding.Common
{
     public class HoldingDto
    {
        public Guid HoldingId;
        public required string Name;
        public int Amount;
    }
}