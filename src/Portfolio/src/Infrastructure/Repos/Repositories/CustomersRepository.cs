

using Portfolio.src.Core.Helpers.Objects;
using Portfolio.src.Infrastructure.Repos.Interfaces;

namespace Portfolio.src.Infrastructure.Repos.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private static List<Customer> customers = new List<Customer>() {
            new Customer(Guid.NewGuid(), "Joshua", "Bloch"),
            new Customer(Guid.NewGuid(), "Mark", "Zuckerberg"),
            new Customer(Guid.NewGuid(), "Martin", "Fowler"),
            new Customer(Guid.NewGuid(), "Lukas", "Grolig"),
            new Customer(Guid.NewGuid(), "Neil", "Armstrong"),
            new Customer(Guid.NewGuid(), "Lance", "Armstrong"),
            new Customer(Guid.NewGuid(), "Mike", "Tyson"),
            new Customer(Guid.NewGuid(), "Marika", "Gombitova"),
        };

        public Customer? Find(Guid id)
        {
            return customers.ToArray().First();//customers.FirstOrDefault(c => c.Id == id);
        }

        public IReadOnlyCollection<Customer> GetCustomers() => customers.AsReadOnly();
    }
}