using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.src.Core.Aggregates.HoldingAggregate;
using Portfolio.src.Infrastructure.Repos.Interfaces;

namespace Portfolio.src.Infrastructure.Repos.Repositories
{
    public class HoldingRepository : IHoldingRepository
    {
        private static List<Holding> holdings = new List<Holding> {
            Holding.NewHolding(new Guid("2766e726-6af5-4451-92b0-729a6f7bac2f"), "Apple holding"),
            Holding.NewHolding(new Guid("03d7e3cd-cb1c-40eb-b510-bd8777dd881e"), "Microsoft holding"),
            Holding.NewHolding(new Guid("4aa3b46c-7c67-4800-bdb2-20481c191366"), "Amazon holding"),
            Holding.NewHolding(new Guid("5ea9cd0b-d2ee-4870-a0d2-4a458ce03023"), "S&P500"),
            Holding.NewHolding(new Guid("c5f17bc6-47fa-461a-95d1-d5c2e275f4a0"), "Google holding"),
            Holding.NewHolding(new Guid("89fbd307-d4fe-44b4-800e-bad490b49022"), "Intel holding"),
            Holding.NewHolding(new Guid("fc0aa89e-7702-41e8-b922-9be9358ac36b"), "BMW holding")
        };

        public Holding Create(Holding holding)
        {
            holdings.Add(holding);
            return holding;
        }

        public Holding? Find(Guid id)
        {
            return holdings.ToArray().First();//holdings.FirstOrDefault(h => h.Id == id);
        }

        public IEnumerable<Holding> GetHoldings()
        {
            return holdings.AsReadOnly();
        }
    }
}