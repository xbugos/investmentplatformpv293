using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.src.Core.Aggregates.HoldingAggregate;

namespace Portfolio.src.Infrastructure.Repos.Interfaces
{
    public interface IHoldingRepository
    {
        public IEnumerable<Holding> GetHoldings();

        public Holding? Find(Guid id);

        public Holding Create(Holding holding);
    }
}