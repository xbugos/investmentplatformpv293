using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.src.Core.Helpers.Objects;


namespace Portfolio.src.Infrastructure.Repos.Interfaces;

public interface ICustomerRepository
{
    public IReadOnlyCollection<Customer> GetCustomers();

    public Customer? Find(Guid id);
}
