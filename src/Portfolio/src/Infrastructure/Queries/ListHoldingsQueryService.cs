using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Threading.Tasks;
using Portfolio.src.Infrastructure.Repos.Interfaces;
using Portfolio.src.UseCases.Holding.List;

namespace Portfolio.src.Infrastructure.Queries
{
    public class ListHoldingsQueryService : IListHoldingsQueryService
    {

        private readonly IHoldingRepository _holdingRepository;

        public ListHoldingsQueryService(IHoldingRepository iHoldingRepoitory)
        {
            _holdingRepository = iHoldingRepoitory;
        }

        public async Task<IEnumerable<HoldingListDto>> ListAsync()
        {
            await Task.Delay(10); 
            return _holdingRepository
                 .GetHoldings()
                 .Select(h => new HoldingListDto(h.Name, h.TotalAmount))
                 .ToList();
        }
    }
}